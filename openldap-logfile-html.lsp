<% local data, viewlibrary = ... 
%>

<% if viewlibrary and viewlibrary.dispatch_component then
	viewlibrary.dispatch_component("alpine-baselayout/logfiles/view", {filename="/var/log/messages", grep="slapd"})
end %>
