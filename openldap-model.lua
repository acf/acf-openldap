local mymodule = {}

modelfunctions = require("modelfunctions")

local processname = "slapd"
local packagename = "openldap"
local configfile = "/etc/openldap/slapd.conf"

function mymodule.getstatus()
	return modelfunctions.getstatus(processname, packagename, "OpenLDAP Status")
end

function mymodule.get_startstop(self, clientdata)       
        return modelfunctions.get_startstop(processname)
end

function mymodule.startstop_service(self, startstop, action)        
        return modelfunctions.startstop_service(startstop, action)
end

function mymodule.get_filecontent()
	return modelfunctions.getfiledetails(configfile)
end

function mymodule.update_filecontent(self, filedetails)
	return modelfunctions.setfiledetails(self, filedetails, {configfile})
end

function mymodule.get_ldapsearch(self)
	local cnf = mymodule.parse_config(configfile)

        local result = {}
        local cmd ="ldapsearch -x -L -b ".. cnf.suffix
	local handle = io.popen(cmd)
	local file = handle:read("*a")
	
	result.suffix = cfe({label="Suffix"})
	result.suffix.value = cnf.suffix or ""
	result.suffix.readonly = true
	result.suffix.seq = 1
	result.dn = cfe({type="longtext", label="DNs"})
	result.dn.seq = 2	

       for line in string.gmatch(file, "[^\n]+") do
                if string.match(line, "^dn") then
			result.dn.value = result.dn.value .."\n"..string.match(line, "dn: (.*)")
        	end        
        end

        return cfe({type="group", value=result, label="Ldap Search Result"})
end


function mymodule.parse_config(configfile)
	local file = io.open (configfile)
	local confs = {}
	
	if (file) then
		local alllines = file:read("*all")
		for line in string.gmatch(alllines, "([^\n]+)") do
			-- Ignoring lines starting by #
			if string.match(line,"^%a") then
				for key, value in string.gmatch(line,"(%w*)%s(.*)") do
					if key then
						confs[key]  = string.gsub(value,"^%s*","")
					end
				end
			end
		end
	end

	file:close()
	return confs
end


return mymodule
