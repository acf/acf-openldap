local mymodule = {}

mymodule.mvc = {}

mymodule.default_action = "status"

mymodule.status = function(self)
	return self.model.getstatus()
end

mymodule.startstop = function(self)
	return self.handle_form(self, self.model.get_startstop, self.model.startstop_service, self.clientdata)
end

mymodule.expert = function(self)
	return self.handle_form(self, self.model.get_filecontent, self.model.update_filecontent, self.clientdata, "Save", "Edit Config File", "Config File Saved")
end

mymodule.search = function(self)
	return self.model.get_ldapsearch(self)
end

return mymodule
